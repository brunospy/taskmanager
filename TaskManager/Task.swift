//
//  Task.swift
//  TaskManager
//
//  Created by Bruno Ribeiro on 8/27/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import CoreData

@objc(Task) // Make compatible with Objective C
class Task: NSManagedObject {

    @NSManaged var name:String
    
    
}
