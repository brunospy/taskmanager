//
//  ViewController.swift
//  TaskManager
//
//  Created by Bruno Ribeiro on 8/24/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var taskNameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var tasks : NSFetchedResultsController {
        if _tasks != nil {
            return _tasks!
        }
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Task", inManagedObjectContext: context)
        
        // Ordenação
        request.sortDescriptors = [NSSortDescriptor(key:"name", ascending: true)]
        
        let result = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        result.delegate = self
        _tasks = result

        do {
            try _tasks!.performFetch()
        } catch {
            print("Ocorreu um erro!")
        }
        
        return _tasks!;
    }
    var _tasks : NSFetchedResultsController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        taskNameTextField.delegate = self
        
        //calcula altura e largura de acordo com o conteúdo da celula
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.registerClass(TaskTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        tableView.backgroundColor = UIColor.frontColor();
        self.view.backgroundColor = UIColor.frontColor();
    }
  
    @IBAction func addTask() {
        if taskNameTextField.text == "" {
            return
        }
        
        let newTask = NSEntityDescription.insertNewObjectForEntityForName("Task", inManagedObjectContext: context) as! Task
        newTask.name = taskNameTextField.text!
        
        saveContext()
        taskNameTextField.text = ""
        taskNameTextField.resignFirstResponder()
    }
    
    //Salva o contexto do banco de dados
    func saveContext(){
        do {
            try context.save()
        } catch {
            print("Ocorreu um erro!")
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return (tasks.fetchedObjects?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! TaskTableViewCell
        
        configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func configureCell(cell:TaskTableViewCell, atIndexPath: NSIndexPath){
        let task = tasks.fetchedObjects![atIndexPath.row] as! Task
        cell.textLabel?.text = task.name;
    }
    
    //delete 
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        if editingStyle == .Delete {
            context.deleteObject(tasks.objectAtIndexPath(indexPath) as! NSManagedObject)
            saveContext()
        }
    }
    
    //UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        addTask()
        return true
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?){
        
        switch type {
            case .Insert :
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Delete :
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Update :
                configureCell(tableView.cellForRowAtIndexPath(newIndexPath!)! as! TaskTableViewCell, atIndexPath: indexPath!)
            default:
                return
        }
        
    }

    
    
}

