//
//  TaskTableViewCell.swift
//  TaskManager
//
//  Created by Bruno Ribeiro on 9/3/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.frontColor()
        self.textLabel?.textColor = UIColor.cellFontColor()
        
        // gesture recognize ocorre quando segura o dedo e arrasta
        let gesture = UIPanGestureRecognizer(target: self, action: "handlePan:")
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func handlePan(recognizer: UIPanGestureRecognizer){
        print("teste");
    }
    
    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        let recognize = gestureRecognizer as! UIPanGestureRecognizer
        let translation = recognize.translationInView(self.superview)
        
        if fabs(translation.x) > fabs(translation.y) {
            return true
        }
        return false
    }

}
